/**
 * Ce programme permet de jouer des parties du jeu de Wythoff joueur contre ordinateur.
 * @author Sarah FERRON
 */
 class Wythoff2 {
	 void principal() {
		 
		 // Appel de la méthode pour jouer au jeu de Wythoff.
		 
		 jouer();
		 
		 // Appel des méthodes de test.
		 
		 testChangeJoueur();
		 testCreerPlateau();
		 testPlacementPion();
		 testCoordonneesPion();
		 testDeplacementPionHumain();
		 testDeplacementPionOrdi();
		 testPositionsGagnantes();
		 testEstGagnante();
		 testFinPartie();
	 }
	 
	 /**
	  * Permet de jouer au jeu de Wythoff.
	  */
	 void jouer() {
		 char direction = ' ';
		 boolean stop = false;
		 int[] coordonnees = new int[2];
		 int taille = SimpleInput.getInt("Veuillez saisir une taille pour le tableau : ");
		 
		 // Vérifie si la taille est inférieure à 100 et redemande si nécessaire.
		 if (taille > 100) {
			taille = SimpleInput.getInt("Veuillez saisir à nouveau une taille pour le tableau : "); 
		 }
		 char[][] wythoff = creerPlateau(taille); // Créer le plateau du jeu.
		 
		 // Initialisation des joueurs.
		 String joueur1 = nomJoueur("joueur 1");
		 String joueur2 = "Ordinateur";
		 String joueurCourant = SimpleInput.getString("Qui joue en premier entre " + joueur1 + " et " + joueur2 + " ? ");
		 
		 placementPion(wythoff); // Place le pion de départ aléatoirement sur le plateau.
		 affichePlateau(wythoff); // Affiche le plateau de départ.
		 
		 //Boucle de jeu
		 while(!stop) {
			 System.out.println("C'est au tour de : " + joueurCourant);
			 coordonnees = coordonneesPion(wythoff); // Récupère les coordonnées du pion.
			 if (joueurCourant.equals(joueur1)) { // Dans ce cas, je dois utiliser "equals" pour comparer les chaînes de caractères car '==' compare les références d'objets.
				 deplacementPionHumain(wythoff,coordonnees); // Le joueur déplace le pion.
			 } else {
				 deplacementPionOrdi(wythoff,coordonnees); // L'ordinateur déplace le pion.
			 }
			 affichePlateau(wythoff);
			 if (!finPartie(wythoff)) {
				 joueurCourant = changeJoueur(joueurCourant, joueur1, joueur2); // Changement de joueur si la partie n'est pas finie
			 } else {
				 stop = true; // Fin de la partie.
			 }
		 }
		 System.out.println("La partie est terminée, le gagnant de la partie est : " + joueurCourant);
	 }
	 
	 /**
	  * Demande à l'utilisateur le nom du joueur.
	  * @param joueur dont le nom est demandé.
	  * @return le nom du joueur.
	  */
	 String nomJoueur(String joueur) {
		 String nom;
		  
		 nom = SimpleInput.getString("Quel est le nom du " + joueur + " ? ");
		  
		 return nom;
	 }

	 /**
	  * Change le joueur courant.
	  * @param joueurCourant le joueur qui vient de terminer son tour.
	  * @param joueur1.
	  * @param joueur2.
	  * @return joueur2 si c'était au tour du joueur1 et inversement.
	  */
	 String changeJoueur(String joueurCourant, String joueur1, String joueur2) {
         if (joueurCourant.equals(joueur1)){ // Dans ce cas, je dois également utiliser "equals" pour comparer les chaînes de caractères car '==' compare les références d'objets.
             joueurCourant = joueur2;
         } else  if (joueurCourant.equals(joueur2)){
             joueurCourant = joueur1;
         }
         return joueurCourant;
     }
     
     /**
      * Créer un plateau de jeu carré rempli de caractères espace ' '
      * @param lg taille du plateau (lg <= 100).
      * @return tableau de caractères en deux dimensions.
      */
     char[][] creerPlateau(int lg) {
		 char[][] plateau = new char [lg][lg];
		 
		 // Initialise le plateau avec seulement des cases vides. 
		 for (int i = 0; i < plateau.length; i++) {
			 for (int j = 0; j < plateau[i].length; j++) {
				 plateau[i][j] = ' ';
			 }
		 }
		 return plateau;
	 }
      
     /**
      * Affichage du plateau de Wythoff avec les indices de lignes et de colonnes
      * @param plateau le tableau à afficher
      */
     void affichePlateau(char[][] plateau) {
		 
		 // Affiche le plateau avec les indices des lignes et de colonnes.
		 
		 for(int j = plateau.length-1; j >= 0; j--) {
			 if (j < 10) {
				 System.out.print(" ");
			 }
			 System.out.print((j) + "|");
			 for(int k = 0; k < plateau.length; k++) {
				 if (plateau[j][k] == 'o') {
					 System.out.print(" " + plateau[j][k] + " |");
				 } else if (estGagnante(plateau,j,k)) { // Affiche les positions gagnantes sur le plateau.
					 System.out.print(" x |");
				 } else {
					 System.out.print(" " + plateau[j][k] + " |");
				 }
			 }
			 System.out.println();
		 }
		 System.out.print("  ");
		 for(int i = 0; i < plateau.length; i++) {
			 System.out.print("  " + i);
			 if (i < 10) {
				 System.out.print(" ");
			 }
		 }
		 System.out.println();
		  
	 }
	  
	 /**
     * Place un pion de manière aléatoire sur les emplacements disponibles.
     * Les emplacements disponibles se trouvent sur la ligne du haut et
     * sur la colonne de gauche du plateau. Le pion ne peut pas se trouver sur les
     * extrémités du plateau.
     * @param plateau le tableau à afficher.
     */
	 void placementPion(char[][] plateau) {
		 // Choisi un indice horizontal de manière aléatoire
		 int horizontal = (int)(Math.random() * (plateau.length-1) + 1);
		 int vertical;
		  
		 // Choisi l'indice vertical en fonction de l'indice horizontal.   
		 if (horizontal < (plateau.length-1)) {
			 vertical = plateau.length-1;
		 } else {  
			 vertical = (int)(Math.random() * (plateau.length-2)+1);
		 }
		 
		 // Place le pion.	 
		 plateau[horizontal][vertical] = 'o';
		 System.out.println("Les coordonnées de la position initiale du pion sont : (" + vertical + "," + horizontal + ")");
	 }
	   
	 /**
	  * Récupère les coordonnées du pion placé.
	  * @param plateau le tableau de jeu.
	  */
	 int[] coordonneesPion(char[][] plateau) {
		 int[] coordonnees = new int[2];
			
		 for(int i = 0; i < plateau.length; i++) {
			 for(int j = 0; j < plateau[i].length; j++) {
				 if(plateau[i][j] == 'o') {
					 coordonnees[0] = i;
					 coordonnees[1] = j;
				 }
			 }
		 }
		 return coordonnees;
	 }
		
	 /**
	  * Permet au joueur humain de déplacer le pion sur le plateau dans les directions suivantes : bas, gauche, diagonal.
	  * @param plateau le tableau de jeu.
	  * @param coordonnes coordonnées du pion.
	  * @return plateau.
	  */
	 char[][] deplacementPionHumain(char[][] plateau,int[]coordonnees) {
		 char direction = ' ';
		 int distanceMax = 0;
		 int nbCases;
		 int horizontal = coordonnees[0];
		 int vertical = coordonnees[1];
		 
		 // Vérifie si le pion n'est pas sur la limite d'une ligne ou d'une colonne.  
		 if (vertical == 0) {
			 direction = 'b';
			 System.out.println("La seule direction possible est vers le bas.");
		 } else if (horizontal == 0) {
			 direction = 'g';
			 System.out.println("La seule direction possible est vers la gauche.");
		 }
		 
		 while (direction != 'b' && direction != 'd' && direction != 'g') {
			 direction = SimpleInput.getChar("Veuillez indiquer la direction dans laquelle vous voulez aller (b pour bas, g pour gauche, d pour diagonal) : ");
		 }
		 
		 // Calcule la distance maximale que le pion peut parcourir en fonction de la direction et de la position du pion.
		 if (direction == 'b') {
			 distanceMax = horizontal;
		 } else if(direction == 'g') {
			 distanceMax = vertical;
		 } else if(direction == 'd') {
			 if(horizontal < vertical) {
				 distanceMax = vertical;
			 } else {
				 distanceMax = horizontal;
			 }
		 }
		 
		 nbCases = SimpleInput.getInt("De combien de cases voulez-vous avancer ? : ");
		 
		 // Si le nombre choisi dépasse du plateau, la question est reposée.
		 while (nbCases > distanceMax || nbCases <= 0) {
			 nbCases = SimpleInput.getInt("Erreur, de combien de cases voulez-vous avancer ? : ");
		 }
		 
		 // Permet d'effacer le pion du tour précédent.
		 plateau[horizontal][vertical] = ' ';
		 
		 // Pose le nouveau pion.	   
		 if (direction == 'b') {
			 plateau[horizontal-nbCases][vertical] = 'o';
		 } else if(direction == 'g') {
			 plateau[horizontal][vertical-nbCases] = 'o';
		 } else if(direction == 'd') {
			 plateau[horizontal-nbCases][vertical-nbCases] = 'o';
		 }
		 return plateau;
	 }
	 
	 /**
	  * Permet à l'ordinateur de déplacer le pion sur le plateau dans les directions suivantes : bas, gauche, diagonal.
	  * @param plateau le tableau de jeu.
	  * @param coordonnes coordonnées du pion.
	  * @return plateau.
	  */
	 char[][] deplacementPionOrdi(char[][] plateau, int[] coordonnees) {
		 int[][] positionsGagnantes = positionsGagnantes(plateau);
		 int nbCases = 0;
		 int horizontal = coordonnees[0];
		 int vertical = coordonnees[1];
		 boolean found = false; // Indique si une position gagnante a été trouvée.
		 int i = 0;
		 
		 plateau[horizontal][vertical] = ' '; // Permet d'effacer le pion du tour précédent.
		 
		 // Parcourt les positions gagnantes possibles pour essayer de trouver un mouvement optimal.
		 while (i < positionsGagnantes.length && !found) {
			 int[] positionGagnante = positionsGagnantes[i];
			 int positionHorizontale = positionGagnante[0];
			 int positionVerticale = positionGagnante[1];
			 
			 // Si les positions gagnantes sont dans la ligne, la colonne et devant le pion.
			 if (positionHorizontale <= horizontal && positionVerticale <= vertical && (positionHorizontale != horizontal || positionVerticale != vertical )) {
				if (horizontal == positionHorizontale || vertical == positionVerticale || (horizontal - vertical) == (positionHorizontale - positionVerticale)) {
					plateau[positionHorizontale][positionVerticale] = 'o';
					found = true; // Position gagnante trouvée, sort de la boucle.
				}
			}
			i = i + 1;	 
		 }
		 
		 // Si aucune position gagnante n'a été trouvée, l'ordinateur effectue un mouvement aléatoire.
		 if (!found) {
			 if (horizontal == 0) {
				 vertical = (int)(Math.random() * vertical);
				 plateau[horizontal][vertical] = 'o';
			 } else if (vertical == 0) {
				 horizontal = (int)(Math.random() * horizontal);
				 plateau[horizontal][vertical] = 'o';
			 } else {
				 // Mouvement aléatoire vertical.
				 if (Math.random() < 1/3) {
					 vertical = (int)(Math.random() * vertical);
					 plateau[horizontal][vertical] = 'o';
				 // Mouvement aléatoire horizontal.
				 } else if (Math.random() < 1/2) {
					 horizontal = (int)(Math.random() * horizontal);
					 plateau[horizontal][vertical] = 'o';
				 // Mouvement aléatoire diagonal.
				 } else {
					 int max = Math.min(horizontal,vertical); // Math.min permet de déterminer la valeur la plus petite entre les deux valeurs.
					 int distance = (int)(Math.random() * max) + 1;
					 plateau[horizontal-distance][vertical-distance] = 'o';
				 }
			 }
		 }
		 
		 return plateau;
		 
	 }
	   
	 /**
	  * Permet de déterminer les coordonnées de toutes les positions gagnantes en fonction de la taille du plateau.
	  * @param plateau le tableau du jeu.
	  * @return positions tableau contenant les coordonnées de toutes les positions gagnantes.
	  */
	 int[][] positionsGagnantes(char[][] plateau) {
		 int[][] positions = new int[plateau.length][2];
		 double phi = 1.61803398875; // Constante définie à la valeur du nombre d'or
		 int x = 0;
		 int y = 0;

		 for (int k = 0; k < plateau.length; k++) {
			 positions[k] = new int[]{x, y};
        
			 // Calcul des prochaines coordonnées en utilisant la formule de Wythoff
			 int prochainX = (int) Math.floor(k * phi); // La fonction Math.floor permet d'arrondir un nombre décimal à l'entier inférieur le plus proche.
			 int prochainY = k + prochainX;
			
			 x = prochainX;
			 y = prochainY;
		 }
		 // Crée un tableau pour les symétries des positions gagnantes
		 int[][] symetriePositions = new int[positions.length][2];

		 // Calcul des coordonnées des symétries
		 for (int k = 0; k < positions.length; k++) {
			 symetriePositions[k][0] = positions[k][1];
			 symetriePositions[k][1] = positions[k][0];
		 }

		 // Crée un tableau pour stocker toutes les positions, originales et symétriques
		 int[][] toutesLesPositions = new int[positions.length * 2][2];

		 // Copie les positions originales dans le tableau combiné
		 for (int k = 0; k < positions.length; k++) {
			 toutesLesPositions[k] = positions[k];
		 }

		 // Copie les positions symétriques dans le tableau combiné
		 for (int k = 0; k < symetriePositions.length; k++) {
			 toutesLesPositions[k + positions.length] = symetriePositions[k];
		 }

		 return toutesLesPositions;
	 }
	 
	 /**
	 * Vérifie si la case est une position gagnante.
	 * @param plateau le tableau de jeu.
	 * @param coordonnees les coordonnées actuelles de la case.
	 * @return true si la case est une position gagnante, false sinon.
	 */
	boolean estGagnante(char[][] plateau, int j, int k) {
		int[][] positionsGagnantes = positionsGagnantes(plateau); // Obtient les positions gagnantes.

		int i = 0;
		boolean gagnante = false;

		while (i < positionsGagnantes.length && !gagnante) {
			if (positionsGagnantes[i][0] == j && positionsGagnantes[i][1] == k) {
				gagnante = true; // La case est une position gagnante.
			}
			i++;
		}
		return gagnante; // true si le pion est dans une position gagnante, sinon false.
	}
	  
	 /**
	  * Termine la partie quand le pion a atteint la case (0,0).
	  * @param plateau le tableau de jeu.
	  * @return fin true si la dernière case est atteinte, false sinon.
	  */
	 boolean finPartie(char[][] plateau) {
		 boolean fin = false;
			
		 if (plateau[0][0] == 'o') {
			 fin = true;
		 }
		 return fin;
	 }
	 
	 // *** METHODES TEST ***
	 
	 /**
     * Teste la méthode changeJoueur.
     */
     void testChangeJoueur() {
		 System.out.println();
         System.out.println("*** testChangeJoueur()");
         // Cas 1: Le joueur courant est joueur1, on s'attend à ce que le joueur courant soit joueur2 après l'appel.
         String joueur1 = "Sarah";
         String joueur2 = "Elias";
         String joueurCourant = "Sarah";

         testCasChangeJoueur(joueurCourant, joueur1, joueur2, "Elias");

         // Cas 2: Le joueur courant est joueur2, on s'attend à ce que le joueur courant soit joueur1 après l'appel.
         String joueur3 = "Mael";
         String joueur4 = "Baptiste";
         String joueurCourant2 = "Baptiste";
         
         testCasChangeJoueur(joueurCourant2, joueur3, joueur4, "Mael");
         
     }
     
     /**
     * Teste un appel de la méthode changeJoueur.
     * @param joueurCourant
     * @param joueur1
     * @param joueur2
     * @param joueurCourantAttendu
     */
     void testCasChangeJoueur(String joueurCourant, String joueur1, String joueur2, String joueurCourantAttendu) {
         // Arrange
         System.out.println("Joueur Courant : " + joueurCourant);
         System.out.print("Résultat attendu : " + joueurCourantAttendu + "\t : ");
         
         // Act
         String resExec = changeJoueur(joueurCourant, joueur1, joueur2);
         
         // Assert
         if (resExec.equals(joueurCourantAttendu)) {
             System.out.println("OK");
         } else {
             System.err.println("ERREUR");
         }
     }
     
     /**
      * Teste la méthode creerPlateau.
      */
     void testCreerPlateau() {
		 System.out.println();
         System.out.println("*** testCreerPlateau()");
         
         // 3 cas différents.
         char[][] t1 = {{' ',' ',' '},{' ',' ',' '},{' ',' ',' '}};
         char[][] t2 = {{' ',' '},{' ',' '}};
         char[][] t3 = {{' '}};
         
         testCasCreerPlateau(3, t1);
         testCasCreerPlateau(2, t2);
         testCasCreerPlateau(1, t3);
	 }
	 
	 /**
	  * Teste un appel de la méthode creerPlateau.
	  * @param lg taille du tableau.
	  * @param char[][] result résultat.
	  */
	 void testCasCreerPlateau(int lg, char[][] result) {
		 //Arrange
		 System.out.println();
         System.out.print("La méthode crée un tableau de taille " + lg + " : ");

         //Act
         char[][] resExec = creerPlateau(lg);
         System.out.println("La méthode affichePlateau() nous permet de le vérifier");
         affichePlateau(resExec);

         //Assert
         if (resExec.length == result.length) {
             System.out.println("OK");
         } else {
             System.err.println("ERREUR");
         }
     }
     
     /**
      * Teste la méthode placementPion.
      */
     void testPlacementPion() {
		 System.out.println();
		 System.out.println("*** testPlacementPion()");
		 
		 // Création des plateaux de test.
		 char[][] t1 = creerPlateau(8);
		 char[][] t2 = creerPlateau(8);
		 char[][] t3 = creerPlateau(8);
		 
		 // On place un pion sur ses plateaux et on vérifie visuellement.
		 placementPion(t1);
		 affichePlateau(t1);
		 
		 placementPion(t2);
		 affichePlateau(t2);
		 
		 placementPion(t3);
		 affichePlateau(t3);
		 
		 System.out.println("Les pions sont bien placés aléatoirement.");
	 }
	 
	 /**
	  * Teste la méthode coordonneesPion.
	  */
	 void testCoordonneesPion() {
		 System.out.println();
		 System.out.println("*** testCoordonneesPion()");
		 
		 // Création des plateaux de test.
		 char[][] t1 = {{' ',' ',' ',' ',' ',' '},
					    {' ',' ','o',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '},
					    {' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '}};
		 affichePlateau(t1);
		 int[] c1 = {1,2};
		 testCasCoordonneesPion(t1,c1);
							  
		 char[][] t2 = {{' ',' ',' ',' ',' ',' '},
					    {' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '},
					    {' ',' ',' ',' ',' ',' '},
						{'o',' ',' ',' ',' ',' '}};
		 affichePlateau(t2);
		 int[] c2 = {5,0};
		 testCasCoordonneesPion(t2,c2);
						
		 char[][] t3 = {{' ',' ',' ',' ',' ',' '},
					    {' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ','o',' '},
					    {' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '}};
		 affichePlateau(t3);
		 int[] c3 = {3,4};
		 testCasCoordonneesPion(t3,c3);
	 }
	 
	 /**
	  * Teste un appel de la méthode coordonneesPion.
	  * @param plateau
	  * @param coordonnees du pion.	
	  */
	 void testCasCoordonneesPion(char[][] plateau, int[] coordonnees) {
		  //Arrange
		 System.out.println();
         System.out.print("La méthode détermine les coordonnées du pion (" + coordonnees[0] + "," + coordonnees[1] + ") : " );

         //Act
         int[] resExec = coordonneesPion(plateau);

         //Assert
         if (resExec[0] == coordonnees[0] && resExec[1] == coordonnees[1]) {
             System.out.println("OK");
         } else {
             System.err.println("ERREUR");
         }
	 }
	 
	 /**
	  * Teste la méthode deplacementPion.
	  */
	 void testDeplacementPionHumain() {
		 System.out.println();
         System.out.println("*** testDeplacementPionHumain");
         
         // 1er cas pour un déplacement vers la gauche.
         char [][] t1 = {{' ',' ',' ',' ',' ',' '},
						 {' ',' ','o',' ',' ',' '},
						 {' ',' ',' ',' ',' ',' '},
						 {' ',' ',' ',' ',' ',' '},
						 {' ',' ',' ',' ',' ',' '},
						 {' ',' ',' ',' ',' ',' '}};
		 System.out.println("Avant : "); 
		 affichePlateau(t1);
         int[] c1 = {1,2};
         int[] r1 = {1,0};
         char d1 = 'g';
         int nbCases1 = 2;
         testCasDeplacementPionHumain(t1, c1, r1, d1, nbCases1);
         
         // 2eme cas pour un déplacement vers le bas.
         char[][] t2 = {{' ',' ',' ',' ',' ',' '},
					    {' ',' ',' ',' ',' ',' '},
						{' ','o',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '},
					    {' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '}};
		 System.out.println("Avant : "); 
		 affichePlateau(t2);
		 int[] c2 = {2,1};
         int[] r2 = {0,1};
         char d2 = 'b';
         int nbCases2 = 2;
         testCasDeplacementPionHumain(t2, c2, r2, d2, nbCases2);
						
         // 3eme cas pour un déplacement en diagonal.
         char[][] t3 = {{' ',' ',' ',' ',' ',' '},
					    {' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ','o',' '},
					    {' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '}};
		 System.out.println("Avant : "); 
		 affichePlateau(t3);
		 int[] c3 = {3,4};
         int[] r3 = {0,1};
         char d3 = 'd';
         int nbCases3 = 3;
         testCasDeplacementPionHumain(t3, c3, r3, d3, nbCases3);
	 }
	 
	 /**
	  * Teste un appel de la méthode deplacementPionHumain.
	  * @param plateau
	  * @param coordonnees du pion
	  * @param result nouvelles coordonnées du pion après déplacement
	  * @param direction
	  * @param nbCases
	  */
	 void testCasDeplacementPionHumain(char[][] plateau, int[] coordonnees, int[] result, char direction, int nbCases) {
		 //Arrange
         System.out.println("La fonction deplacementPion deplace le pion aux coordonnées (2,4) à (" + result[0] + "," + result[1] + ") avec un input (" + direction + ",10) puis : (" + direction + "," + nbCases +")");

         //Act
         deplacementPionHumain(plateau, coordonnees);
         coordonnees = coordonneesPion(plateau);
         int [] resExec = coordonnees;
         
         System.out.println("Après : "); 
		 affichePlateau(plateau);

         //Assert
         if (resExec[0] == result[0] && resExec[1] == result[1]) {
             System.out.println("OK");
         } else {
             System.err.println("ERREUR");
         }
	 }
	 
	 /**
	  * Teste la méthode finPartie.
	  */
	 void testFinPartie() {
		 System.out.println();
         System.out.println("*** testFinPartie");
         
         // 1er cas : partie pas terminée.
		 char[][] t1 = {{' ',' ',' ',' ',' ',' '},
					    {' ',' ','o',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '},
					    {' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '}};
		 affichePlateau(t1);
		 testCasFinPartie(t1, false);
		 
		 // 2ème cas : partie terminée.
		 char[][] t2 = {{'o',' ',' ',' ',' ',' '},
					    {' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '},
					    {' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '}};
		 affichePlateau(t2);
		 testCasFinPartie(t2, true);
	 }
	 
	 /**
	  * Teste un appel de la méthode finPartie.
	  * @param plateau
	  * @param result vrai si la partie est terminée.
	  */
	 void testCasFinPartie(char[][] plateau,boolean result) {
		 //Arrange
         System.out.println("La partie est-elle terminée ? \t= " + result + "\t: ");

         //Act
         boolean resExec = finPartie(plateau);

         //Assert
         if (resExec == result) {
             System.out.println("OK");
         } else {
             System.err.println("ERREUR");
         }
	 }
	 
	 /**
	  * Teste un appel de la méthode deplacementPionOrdi.
	  */
	 void testDeplacementPionOrdi() {
		 System.out.println();
         System.out.println("*** testDeplacementPionOrdi");
         
         char [][] t1 = {{' ',' ',' ',' ',' ',' '},
						 {' ',' ','o',' ',' ',' '},
						 {' ',' ',' ',' ',' ',' '},
						 {' ',' ',' ',' ',' ',' '},
						 {' ',' ',' ',' ',' ',' '},
						 {' ',' ',' ',' ',' ',' '}};
		 System.out.println("Avant : "); 
		 affichePlateau(t1);
         int[] c1 = {1,2};
         
         deplacementPionOrdi(t1, c1);
         System.out.println("Après : "); 
		 affichePlateau(t1);
         
         char[][] t2 = {{' ',' ',' ',' ',' ',' '},
					    {' ',' ',' ',' ',' ',' '},
						{' ','o',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '},
					    {' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '}};
		 System.out.println("Avant : "); 
		 affichePlateau(t2);
		 int[] c2 = {2,1};
						
		 deplacementPionOrdi(t2,c2);
		 System.out.println("Après : "); 
		 affichePlateau(t2);
		 
         char[][] t3 = {{' ',' ',' ',' ',' ',' '},
					    {' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ','o',' '},
					    {' ',' ',' ',' ',' ',' '},
						{' ',' ',' ',' ',' ',' '}};
		 System.out.println("Avant : "); 
		 affichePlateau(t3);
		 int[] c3 = {3,4};
		 
		 deplacementPionOrdi(t3,c3);
		 System.out.println("Après : "); 
		 affichePlateau(t3);

	 }
	 
	 /**
	  * Teste la méthode positionsGagnantes()
	  */
	 void testPositionsGagnantes() {
		 System.out.println();
         System.out.println("*** testPositionsGagnantes");
         
         // On initialise un tableau comptenant les coordonnées des positions gagnantes.
         int[][] result = {{0,0},{1,2},{3,5},{4,7},{6,10},{0,0},{2,1},{5,3},{7,4},{10,6}};
         char[][] plateau;
         
         plateau = creerPlateau(10);
         affichePlateau(plateau);
         testCasPositionsGagnantes(plateau,result);
	 }
	 
	 /**
	  * Teste un appel de la méthode positionsGagnantes()
	  * @param plateau
	  * @param result
	  */
	 void testCasPositionsGagnantes(char[][] plateau, int[][] result) {
		 // Arrange
		 System.out.println("Les positions gagnantes sont-elles bien placées ?");
		 
		 //Act
         int[][] resExec = positionsGagnantes(plateau);

         //Assert
         if (resExec.length != result.length) {
             System.out.println("OK");
         } else {
             System.err.println("ERREUR");
         }
	 }
	 
	 /**
	  * Teste la méthode estGagnante.
	  */
	 void testEstGagnante() {
		 System.out.println();
         System.out.println("*** testEstGagnante");
         
         // 1er cas : la case est gagnante
         char[][] t1;
         t1 = creerPlateau(10);
         t1[1][2] = 'o';
         affichePlateau(t1);
         
         int h1 = 1;
         int v1 = 2;
         
         testCasEstGagnante(t1, h1, v1, true);
         
         // 2ème cas : la case n'est pas gagnante
         char[][] t2;
         t2 = creerPlateau(10);
         t2[1][3] = 'o';
         affichePlateau(t2);
         
         int h2 = 1;
         int v2 = 3;
         
         testCasEstGagnante(t2, h2, v2, false);
	 }
	 
	 /**
	  * Teste un appel de la méthode estGagnante.
	  * @param plateau
	  * @param h coordonnée horizontale
	  * @param v coordonnée verticale
	  * @param result
	  */
	 void testCasEstGagnante(char[][] plateau, int h, int v,boolean result) {
		 // Arrange
		 System.out.println("Le pion est-il sur une position gagnante ?");
		 
		 //Act
         boolean resExec = estGagnante(plateau, h, v);

         //Assert
         if (resExec == result) {
             System.out.println("OK");
         } else {
             System.err.println("ERREUR");
         }
	 }
}
